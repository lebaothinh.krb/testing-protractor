import { Component, OnInit } from '@angular/core';
import { Student } from './student';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  hidden: boolean;
  status: boolean;
  studentEx: Student;
  errors: string = '';
  arrStudents = [
    new Student(1, "Lê Bảo Thịnh", "true", new Date("3/1/1997"), "Việt Nam", "D15PM02"),
    new Student(2, "Nguyễn Công Thành", "true", new Date("3/2/1997"), "Ấn Độ", "D15PM02"),
    new Student(3, "Hoàng Minh Tú", "true", new Date("8/11/1997"), "Lào", "D15PM02"),
    new Student(4, "Võ Xuân Phúc", "true", new Date("12/30/1997"), "Campuchia", "D15PM01"),
    new Student(5, "Phạm Thanh Hằng", "false", new Date("7/2/1997"), "Ả Rập", "D15PM02")
  ];

  ngOnInit(): void {
    this.hidden = false;
    this.status = false;
    this.errors = '';
  }

  OpenAddStudent() {
    this.hidden = true;
    this.status = true;
    this.studentEx = new Student(this.arrStudents[this.arrStudents.length - 1].IDStudent + 1, null, "true", null, null, "D15PM01");
  }
  Validator(student: Student): boolean{
    if (student.studentName == null){
      this.errors = "Chưa nhập tên sinh viên";
      return false;
    }
    if (student.dateOfBirth == null){
      this.errors = "Chưa nhập ngày sinh";
      return false;
    }
    if (student.address == null){
      this.errors = "Chưa nhập địa chỉ";
      return false;
    }
    if (new Date(student.dateOfBirth) < new Date("1/1/1990")){
      this.errors = "Ngày sinh phải từ 01/01/1990 -> 31/12/2012";
      return false;
    }
    if (new Date(student.dateOfBirth) >= new Date("1/1/2012")){
      this.errors = "Ngày sinh phải từ 01/01/1990 -> 31/12/2012";
      return false;
    }
    return true;
    
  }
  AddStudent(student: Student){
    console.log(new Date(student.dateOfBirth) + " - "+ new Date("1/1/2013"));
    if (this.Validator(student) == true){
      if (this.AddStudentFromServer(student) == true) {
        alert("Thêm Thành Công!");
        this.ngOnInit();
      }
      else alert("Thêm Thất Bại!");
    };
  }

  UpdateStudent(student: Student)
  {
    if (this.UpdateStudentFromServer(student) == true){
      alert("Cập Nhật Thành Công!");
      this.ngOnInit();
    }
    else alert("Cập Nhật Thất Bại!");
  }

  DeleteStudent(IDStudent: number){
    if (this.DeleteStudentFromServer(IDStudent)==true){
      alert("Xóa Thành Công!");
    }
    else alert("Xóa Thất Bại!");
  }

  OpenUpdateStudent(IDStudent: number) {
    this.status = false;
    this.hidden = true;
    this.studentEx = new Student(null,null,null,null,null,null);
    this.arrStudents.forEach(element => {
      if (element.IDStudent == IDStudent) {
        this.studentEx.IDStudent = element.IDStudent;
        this.studentEx.studentName = element.studentName;
        this.studentEx.dateOfBirth = element.dateOfBirth;
        this.studentEx.sex = element.sex;
        this.studentEx.class = element.class;
        this.studentEx.address = element.address;
        return;
      }
    });
  }

  AddStudentFromServer(student: Student): boolean {
    if (this.arrStudents.push(student) != null) {
      return true;
    }
    return false;
  }

  UpdateStudentFromServer(student: Student) {
    for (let i = 0; i < this.arrStudents.length; i++) {
      if (this.arrStudents[i].IDStudent == student.IDStudent) {
        this.arrStudents[i].studentName = student.studentName;
        this.arrStudents[i].address = student.address;
        this.arrStudents[i].class = student.class;
        this.arrStudents[i].dateOfBirth = student.dateOfBirth;
        this.arrStudents[i].sex = student.sex;
        return true;
      }
    };
    return false;
    //console.log(student);
  }

  DeleteStudentFromServer(IDStudent: number){
    for (let i = 0; i < this.arrStudents.length; i++) {
      if (this.arrStudents[i].IDStudent == IDStudent) {
        this.arrStudents.splice(i,1);
        return true;
      }
    };
    return false;
  }
}
