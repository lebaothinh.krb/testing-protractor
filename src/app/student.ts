export class Student{
    public constructor(IDStudent: number, studentName: string,sex: string,dateOfBirth: Date,address: string, classs: string){
        this.IDStudent = IDStudent;
        this.studentName = studentName;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.class = classs;
    }
    IDStudent: number;
    studentName: string;
    sex: string;
    dateOfBirth: Date;
    address: string;
    class: string;
}